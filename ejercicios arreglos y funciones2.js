alumno=[
    {"num.control":"1973842","nombrec":"javier hernandez","grupo":"1111"},
    {"num.control":"7630173","nombrec":"hector moreno","grupo":"1111"},
    {"num.control":"2637328","nombrec":"guillermo ochoa","grupo":"1111"},
    {"num.control":"7643321","nombrec":"carlos vela","grupo":"1111"},
    {"num.control":"8743721","nombrec":"andres guardado","grupo":"1111"},
    {"num.control":"3840207","nombrec":"javier aquino","grupo":"1111"},
    {"num.control":"6540009","nombrec":"oribe peralta","grupo":"1111"},
    {"num.control":"5436772","nombrec":"rafael marquez","grupo":"1111"},
    {"num.control":"9990432","nombrec":"miguel layun","grupo":"1111"},
    {"num.control":"7954821","nombrec":"miguel ponce","grupo":"1111"}
    
    ];
 
materia=[
    {"clave":"his74","nombre":"historia"},
    {"clave":"cie63","nombre":"ciencias naturales"},
    {"clave":"fis08","nombre":"fisica general"},
    {"clave":"mat56","nombre":"matematicas"},
    {"clave":"esp12","nombre":"español"},
    {"clave":"qui77","nombre":"quimica general"},
    {"clave":"geo99","nombre":"geografia"}
    
    ];
    
profesor=[
    {"clave":"p77392","nombrep":"felipe morales"},
    {"clave":"p12345","nombrep":"martin cortez"},
    {"clave":"p58301","nombrep":"jose martinez"},
    {"clave":"p43800","nombrep":"manuel garcia"},
    {"clave":"p53781","nombrep":"hector fabian"},
    {"clave":"p76548","nombrep":"cristian valdez"},
    {"clave":"p68432","nombrep":"jorge campos"}
    
    ];
    
horario=[
    {"hora incial":"7:00","hora final":"8:00","cmateria":"his74","cprofesor":"p77392"},
    {"hora incial":"8:00","hora final":"9:00","cmateria":"cie63","cprofesor":"p12345"},
    {"hora incial":"9:00","hora final":"10:00","cmateria":"fis08","cprofesor":"p58301"},
    {"hora incial":"10:00","hora final":"11:00","cmateria":"mat56","cprofesor":"p43800"},
    {"hora incial":"11:00","hora final":"12:00","cmateria":"esp12","cprofesor":"p53781"},
    {"hora incial":"12:00","hora final":"13:00","cmateria":"qui77","cprofesor":"p76548"},
    {"hora incial":"13:00","hora final":"14:00","cmateria":"geo99","cprofesor":"p68432"}
    
   ];

   
grupo={
    "clave":"473993",
    "nombres":"ISA",
    "alumnos":alumno,
    "materias":materia,
    "profesores":profesor,
    "horarios":horario
};


 

/*funcion que dado un grupo, imprima una lista de alumnos*/
function imprimir_lista_alumnos(obj_grupo)
{
    for(var i=0;i<obj_grupo["alumnos"].length; i++)
    {
        console.log(obj_grupo["alumnos"][i]["nombrec"]);
    }
}

console.log(imprimir_lista_alumnos(grupo));

/*funcion que dado un grupo,imprima la relacion de horario-profesor-materia*/
function imprimir_lista_horario(obj_grupo)
{
    var lectura;
    for(var i=0; i<obj_grupo["horarios"].length;i++ )
    {
        lectura=obj_grupo["horarios"][i]["hora incial"][i];
        console.log(lectura);
    }
}

console.log(imprimir_lista_horario(grupo));


/*funcion buscar si alumno esta en el grupo*/
function imprimir_alumno(grupo,numc)
{
    var existe=false;
    for(var i=0; i<grupo["alumnos"].length;i++)
    {
        if(numc==grupo["alumnos"][i]["num.control"])
        {
            existe=true;
        }
        
    }
    return existe;
}

/*funcion buscar si maestro esta en el grupo*/
function imprimir_profesor(grupo,numc)
{
    var existe=false;
    for(var i=0; i<grupo["profesores"].length;i++)
    {
        if(numc==grupo["profesores"][i]["clave"])
        {
            existe=true;
        }
        
    }
    return existe;
}

/*funcion buscar si materia esta en el grupo*/
function imprimir_materia(grupo,numm)
{
    var existe=false;
    for(var i=0; i<grupo["materias"].length;i++)
    {
        if(numm==grupo["materias"][i]["clave"])
        {
            existe=true;
        }
        
    }
    return existe;
}

console.log(imprimir_alumno(grupo,"2637328"));


/*insertar nuevo alumno*/
function ingresar_nuevo_alumno(grupo,nconn,nombn,grupn)
{
    var existe;
   if(imprimir_alumno(grupo,nconn)==false)
   {
       alumno.push({"num.control":nconn,"nombrec":nombn,"grupo":grupn});
       existe=true;
   }
   else
   {
      existe=false;
   }
     
     return existe;

}

console.log(ingresar_nuevo_alumno(grupo,"8657492","javier sanchez","1111"));

/*insertar nuevo maestro*/
function ingresar_nuevo_maestro(grupo,clavep,nombrep)
{
    var existe;
   if(imprimir_profesor(grupo,clavep)==false)
   {
       profesor.push({"clave":clavep,"nombrep":nombrep});
       existe=true;
   }
   else
   {
      existe=false;
   }
     
     return existe;

}

console.log(ingresar_nuevo_maestro(grupo,"p006523","jacinto martinez"));

/*insertar nueva materia*/
function ingresar_nueva_materia(grupo,clavem,nombrem)
{
    var existe;
   if(imprimir_materia(grupo,clavem)==false)
   {
       materia.push({"clave":clavem,"nombre":nombrem});
       existe=true;
   }
   else
   {
      existe=false;
   }
     
     return existe;

}

console.log(ingresar_nueva_materia(grupo,"edu75492","educacion fisica"));

function eliminar_alumno(grupo,nconn,nombn,grupn)
{
    var existe;
   if(imprimir_alumno(grupo,nconn)==true)
   {
      alumno.splice(0,1,2,{"num.control":nconn,"nombrec":nombn,"grupo":grupn}); 
       existe=false;
   }
   else
   {
      
   }
     
     return existe;

}

console.log(eliminar_alumno(grupo,"8657492","javier sanchez","1111"));
